window.addEventListener("keydown", (event) => {
  switch (event.key) {
    case "q":
      player1.velocity.y = -4
      break
    case "a":
      player1.velocity.y = 4
      break
    case "ArrowUp":
      player2.velocity.y = -4
      break
    case "ArrowDown":
      player2.velocity.y = 4
      break
  }
})

window.addEventListener("keyup", (event) => {
  switch (event.key) {
    case "q":
      player1.velocity.y = 0
      break
    case "a":
      player1.velocity.y = 0
      break
    case "ArrowUp":
      player2.velocity.y = 0
      break
    case "ArrowDown":
      player2.velocity.y = 0
      break
    case "Enter":
      if (pause) {
        info.textContent = "starting new round"
        setTimeout(() => {
          reset()
        }, 1000)
      }
      break
  }
})

document.addEventListener("touchstart", handleTouchStart, false)

function handleTouchStart(event) {
  let tx = event.touches[0].clientX
  let ty = event.touches[0].clientY
  if (pause) {
    info.textContent = "starting new game"
    setTimeout(() => {
      reset()
    }, 1000)
    return
  }

  if (tx <= window.innerWidth / 2 && ty <= window.innerHeight / 2) {
    player1.velocity.y = -4
  }
  if (tx <= window.innerWidth / 2 && ty >= window.innerHeight / 2) {
    player1.velocity.y = 4
  }
  if (tx >= window.innerWidth / 2 && ty <= window.innerHeight / 2) {
    player2.velocity.y = -4
  }
  if (tx >= window.innerWidth / 2 && ty >= window.innerHeight / 2) {
    player2.velocity.y = 4
  }
}
