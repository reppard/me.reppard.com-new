var delay_timer
var storage = window.localStorage

var versions = {
  "KJV": "kjv.json",
  "ASV": "asv.json",
  "CRTB": "crtb.json",
}

var bible_data = []
var search_data = []
var ot_books = []
var nt_books = []
var current_book = []
var current_chapter = []
var current_version = null
var current_book_chapters = []
var show_books = false
var show_chapters = false

var ot = document.querySelector(".ot_books")
var nt = document.querySelector(".nt_books")
var book_btn = document.querySelector(".book_btn")
var content_header = document.querySelector(".content_header")
var chapter_header = document.querySelector(".chapter_header")
var rand_btn = document.querySelector(".random_btn")
var books_elm = document.querySelector(".books")
var chapter_btn = document.querySelector(".chapter_btn")
var chapters_elm = document.querySelector(".chapters")
var loading_elm = document.querySelector(".loading")
var data_elm = document.querySelector(".data")
var results_elm = document.querySelector(".results")
var search_input_elm = document.querySelector(".search input")
var version_elm = document.querySelector(".version")

book_btn.onclick = () => {
  toggle_books()
}

chapter_btn.onclick = () => {
  toggle_chapters()
}

rand_btn.onclick = () => {
  let nt = Math.floor(Math.random() * 2)
  let book

  if (nt) {
    book = nt_books[Math.floor(Math.random() * nt_books.length)]
  } else {
    book = ot_books[Math.floor(Math.random() * nt_books.length)]
  }

  set_book(book)

  let chapter = Math.floor(Math.random() * current_book_chapters.length + 1)
  set_chapter(chapter)

  let verse = Math.floor(Math.random() * current_chapter.length + 1)

  console.log(current_chapter[verse])
  window.location.hash = `#${book}:${chapter}:${verse}`
}

function handle_pager_prev() {
  let curr_chapter = parseInt(storage.getItem("current_chapter"))
  let curr_book = storage.getItem("current_book")
  let prev = current_book_chapters.indexOf(curr_chapter - 1)

  if (prev >= 0) {
    window.location.hash = `#${curr_book}:${prev + 1}`
  } else {
    console.log("prev: " + prev)

    let ot_index = ot_books.indexOf(curr_book)
    let nt_index = nt_books.indexOf(curr_book)

    if (ot_index >= 0) {
      if (ot_books[ot_index - 1]) {
        set_book(ot_books[ot_index - 1])
        window.location.hash = `#${ot_books[ot_index - 1]}:${
          current_book_chapters.length
        }`
      } else {
        set_book(nt_books[nt_books.length - 1])
        window.location.hash = `#${nt_books[nt_books.length - 1]}:${
          current_book_chapters.length
        }`
      }
    } else if (nt_books[nt_index - 1]) {
      set_book(nt_books[nt_index - 1])
      window.location.hash = `#${nt_books[nt_index - 1]}:${
        current_book_chapters.length
      }`
    } else {
      set_book(ot_books[ot_books.length - 1])
      window.location.hash = `#${ot_books[ot_books.length - 1]}:${
        current_book_chapters.length
      }`
    }
  }

  window.scrollTo(0, 0)
}

function handle_pager_next() {
  let curr_chapter = parseInt(storage.getItem("current_chapter"))
  let curr_book = storage.getItem("current_book")
  let next = current_book_chapters.indexOf(curr_chapter + 1)

  if (next >= 0) {
    window.location.hash = `#${curr_book}:${next + 1}`
  } else {
    let ot_index = ot_books.indexOf(curr_book)
    let nt_index = nt_books.indexOf(curr_book)

    if (ot_index >= 0) {
      if (ot_books[ot_index + 1]) {
        window.location.hash = `#${ot_books[ot_index + 1]}:1`
      } else {
        window.location.hash = `#${nt_books[0]}:1`
      }
    } else if (nt_books[nt_index + 1]) {
      window.location.hash = `#${nt_books[nt_index + 1]}:1`
    } else {
      window.location.hash = `#${ot_books[0]}:1`
    }
  }

  window.scrollTo(0, 0)
}

function handle_version() {
  console.log(
    "setting version: " + version_elm.options[version_elm.selectedIndex].value
  )
  set_version(version_elm.options[version_elm.selectedIndex].value)
}

function handle_search() {
  clearTimeout(delay_timer)

  delay_timer = setTimeout(function () {
    let srch = search_input_elm.value.toLowerCase()

    search_data = bible_data.filter((x) =>
      JSON.stringify(x).toLowerCase().match(srch)
    )

    if (srch.length > 2 && search_data.length > 0) {
      data_elm.style.display = "none"
      results_elm.style.display = "block"
    } else {
      data_elm.style.display = "block"
      results_elm.style.display = "none"
    }

    set_results()
  }, 1000)
}

function reset_search() {
  search_data = []
  search_input_elm.value = ""
  results_elm.innerHTML = ""
  data_elm.style.display = "block"
  results_elm.style.display = "none"
}

function set_results() {
  results_elm.innerHTML = ""
  for (let i in search_data) {
    let verse = document.createElement("div")
    verse.className = "result"

    let verse_string = `${search_data[i].book_name}:${search_data[i].chapter}:${search_data[i].verse}`
    verse.innerHTML = `
            <div class="result-wrapper">
              <div class="result-text">
                <p>
                  ${search_data[i].text}
                </p>
              </div>
              <div class="result-details">
                <h3>
                  <a href="#${verse_string}">
                    ${verse_string}
                  </a>
                </h3>
              </div>
            </div>
          `
    results_elm.appendChild(verse)
  }
}

function toggle_books() {
  show_books = !show_books

  if (show_chapters && show_books) {
    toggle_chapters()
  }

  if (show_books) {
    books_elm.classList.add("fadein")
    books_elm.classList.remove("fadeout")
  } else {
    books_elm.classList.remove("fadein")
    books_elm.classList.add("fadeout")
  }
}

function toggle_chapters() {
  show_chapters = !show_chapters

  if (show_books && show_chapters) {
    toggle_books()
  }

  if (show_chapters) {
    chapters_elm.classList.add("fadein")
    chapters_elm.classList.remove("fadeout")
  } else {
    chapters_elm.classList.remove("fadein")
    chapters_elm.classList.add("fadeout")
  }
}

function set_loading(loading) {
  if (loading) {
    loading_elm.style.display = "block"
    data_elm.style.display = "none"
  } else {
    loading_elm.style.display = "none"
    data_elm.style.display = "block"
  }
}

async function grab_data() {
  set_loading(true)
  let current_version = storage.getItem("current_version")
    ? storage.getItem("current_version")
    : "kjv.json"
  let resp = await fetch(current_version).then((resp) => resp.json())

  return resp
}

function set_chapter(chapter) {
  reset_search()
  chapter_btn.innerHTML = "<p>" + chapter + "</p>"
  chapter_header.innerHTML = "<h2>Chapter " + chapter + "</h2>"
  storage.setItem("current_chapter", chapter)

  data_elm.innerHTML = ""
  current_chapter = current_book.filter((x) => x.chapter == chapter)

  for (let i in current_chapter) {
    let verse = document.createElement("div")
    verse.className = "verse"

    verse.innerHTML = `
            <div class="verse-wrapper">
              <div class="verse-number">
                <p>
                  <a href="#${current_chapter[i].book_name}:${current_chapter[i].chapter}:${current_chapter[i].verse}">
                  ${current_chapter[i].verse}
                  </a>
                </p>
              </div>

              <div class="verse-text">
                <p>
                  <a href="#${current_chapter[i].book_name}:${current_chapter[i].chapter}:${current_chapter[i].verse}">
                  ${current_chapter[i].text}
                  </a>
                </p>
              </div>
            </div>
          `
    data_elm.appendChild(verse)
  }
}

function set_book(book) {
  reset_search()
  content_header.innerHTML = "<h4>" + book + "</h4>"
  book_btn.innerHTML = "<p>" + book + "</p>"
  storage.setItem("current_book", book)

  current_book_chapters = []
  current_book = bible_data.filter((x) => x.book_name == book)

  for (let i in current_book) {
    if (current_book[i].verse == 1) {
      current_book_chapters.push(current_book[i].chapter)
    }
  }

  chapters_elm.innerHTML = ""

  for (let i in current_book_chapters) {
    let new_button = document.createElement("button")

    new_button.innerHTML = "<p>" + current_book_chapters[i] + "</p>"

    new_button.onclick = () => {
      window.location.hash = `#${book}:${parseInt(i) + 1}`
      toggle_chapters()
    }

    chapters_elm.appendChild(new_button)
  }

  window.location.hash = `#${book}`
  set_chapter(current_book_chapters[0])
}

function set_version(version) {
  storage.setItem("current_version", version)
  current_version = storage.getItem("current_version")
  init()
}

function setup_version_options() {
  let version = storage.getItem("current_version")

  if (!version || version == "undefined" || version == undefined) {
    storage.setItem("current_version", versions["KJV"])
  }

  current_version = storage.getItem("current_version")

  version_elm.innerHTML = ""

  for (let i in versions) {
    let new_option = document.createElement("option")

    new_option.value = versions[i]
    new_option.innerHTML = i

    version_elm.appendChild(new_option)
  }

  for (var i, j = 0; (i = version_elm.options[j]); j++) {
    if (i.value == current_version) {
      version_elm.selectedIndex = j
      break
    }
  }

  console.log(versions)
}

function init() {
  window.addEventListener("popstate", handle_param)

  grab_data().then((data) => {
    bible_data = data

    if (ot_books.length <= 0) {
      for (let i in data) {
        if (data[i].chapter == 1 && data[i].verse == 1) {
          if (ot_books.length < 39) {
            ot_books.push(data[i].book_name)
          } else {
            nt_books.push(data[i].book_name)
          }
        }
      }

      for (let i in ot_books) {
        let new_button = document.createElement("button")

        new_button.innerHTML = "<p>" + ot_books[i] + "</p>"
        new_button.onclick = () => {
          set_book(ot_books[i])
          toggle_books()
        }

        ot.appendChild(new_button)
      }

      for (let i in nt_books) {
        let new_button = document.createElement("button")

        new_button.innerHTML = "<p>" + nt_books[i] + "</p>"
        new_button.onclick = () => {
          set_book(nt_books[i])
          toggle_books()
        }

        nt.appendChild(new_button)
      }
    }

    let current_book = storage.getItem("current_book")
      ? storage.getItem("current_book")
      : "Genesis"

    let current_chapter = storage.getItem("current_chapter")
      ? storage.getItem("current_chapter")
      : 1

    set_book(current_book)
    set_chapter(current_chapter)
    setup_version_options()
    set_loading(false)
    handle_param()
  })
}

function formatBookName(data) {
  let book = data.replace(/%20/g, " ")

  let ot_search = ot_books.filter((x) => x.toLowerCase() == book.toLowerCase())
  let nt_search = nt_books.filter((x) => x.toLowerCase() == book.toLowerCase())

  console.log("book:" + book)
  console.log("ot_search:" + ot_search)
  console.log("nt_search:" + nt_search)

  if (ot_search.length > 0) {
    return ot_search[0]
  }

  if (nt_search.length > 0) {
    return nt_search[0]
  }

  return ""
}

function handle_param() {
  let params = window.location.hash.split("#")

  if (params.length > 1) {
    console.log("parsing...")

    let split = params[1].split(":")

    let book = formatBookName(split[0])

    let chapter = split[1]
    let verse = split[2]

    set_book(book)
    set_chapter(parseInt(chapter) || 1)

    let current_book = bible_data.filter((x) => x.book_name == book)

    let search = current_book.filter(
      (x) => x.verse == parseInt(verse) && x.chapter == parseInt(chapter)
    )

    if (search.length > 0) {
      data_elm.innerHTML = ""
      content_header.innerHTML = ""
      chapter_header.innerHTML = ""
      chapter_header.innerHTML = `
                <div class="verse-details">
                  <p>
                    <b>
                    ${search[0].book_name}
                    ${search[0].chapter}:${search[0].verse} (${search[0].translation_id})
                    </b>
                  </p>
                </div>
      `

      let verse_elm = document.createElement("div")
      verse_elm.className = "verse"

      verse_elm.innerHTML = `
              <div class="verse-wrapper">
                <div class="verse-text">
                  <h3>
                    "${search[0].text}"
                  </h3>
                </div>
              </div>
            `
      data_elm.appendChild(verse_elm)
    }
  }
}

init()
