const b = document.getElementById("background");
const t = document.getElementById("target");
const o = document.getElementById("overlay");
const s = document.getElementById("sprite");
const i = document.getElementById("impact");
const box = document.querySelector(".box");
const ol = document.querySelector(".overlay");

b.width = box.clientWidth;
b.height = box.clientHeight;
t.width = box.clientWidth;
t.height = box.clientHeight;
o.width = box.clientWidth;
o.height = box.clientHeight;
s.width = box.clientWidth;
s.height = box.clientHeight;
i.width = box.clientWidth;
i.height = box.clientHeight;

const bctx = b.getContext("2d");
const tctx = t.getContext("2d");
const octx = o.getContext("2d");
const sctx = s.getContext("2d");
const ictx = i.getContext("2d");

const audioContext = getAudioContext();
const debug = document.getElementById("debug");
const scorebox = document.getElementById("score");
const startbtn = document.getElementById("start");
const fill = document.querySelector(".bar-fill");
const tankUrl = "img/tank.png";
const imgUrl = "img/bomb.png";
const bkgrdUrl = "img/background.jpg";
//const colors = ["yellow", "red", "green","blue","purple","orange"]
const colors = ["white"];

const sounds = [
  {
    tone: 120.0,
  },
  {
    tone: 161.63,
  },
  {
    tone: 193.66,
  },
  {
    tone: 129.63,
  },
];

var ratio = null;

if (box.clientWidth < box.clientHeight) {
  ratio = box.clientWidth / box.clientHeight;
} else {
  ratio = box.clientHeight / box.clientWidth;
}

var oscMain = null;
var gainMain = null;
var volume = 60;
var color = colors[Math.floor(Math.random() * colors.length)];

var pieceSzx = 25 * ratio;
var pieceSzy = 50 * ratio;
var targetSzx = 100 * ratio;
var targetSzy = 50 * ratio;

var lvl = 1;
var ystep = 2;
var score = 0;
var life = Math.floor(25 * lvl);
var bombs = Math.floor((25 * lvl) / 2);
var started = false;
var x = o.width / 2 - pieceSzx / 2;
var y = 0;

var tx = Math.floor(Math.random() * t.width - targetSzy);
var tox = Math.floor(Math.random() * t.width - targetSzy);

var step = 0;
var img = new Image();
var bk = new Image();
var tank = new Image();

function scoreBoxTemplate() {
  return `
  <div class="scorebox-item">
    BOMBS:${bombs} 
  </div>
  <div class="scorebox-item">
    LVL:${lvl} 
  </div>
  <div class="scorebox-item">
    SCORE:${score} 
  </div>
  `;
}

img.src = imgUrl;
bk.src = bkgrdUrl;
tank.src = tankUrl;
scorebox.innerHTML = scoreBoxTemplate();
drawPlayerPiece(x, y);
drawTarget();
drawBackground();

function playOSC(freq = 440.0, stop = 0.02, type = "triangle") {
  if (oscMain != null) {
    oscMain.stop();
    oscMain = null;
  }

  oscMain = null;
  oscMain = audioContext.createOscillator();

  oscMain.type = type;
  oscMain.frequency.value = freq;

  gainMain = gainMain || audioContext.createGain();
  gainMain.gain.value = volume / 100.0;

  oscMain.connect(gainMain);
  gainMain.connect(audioContext.destination);

  oscMain.start();
  oscMain.stop(audioContext.currentTime + stop);
}

function getAudioContext() {
  let ctx = null,
    usingWebAudio = true;

  try {
    // Fix up for prefixing
    window.AudioContext = window.AudioContext || window.webkitAudioContext;
    ctx = new AudioContext();

    // context state at this time is `undefined` in iOS8 Safari
    if (usingWebAudio && ctx.state === "suspended") {
      var resume = function () {
        ctx.resume();

        setTimeout(function () {
          if (ctx.state === "running") {
            document.body.removeEventListener("touchend", resume, false);
          }
          step = 0;
        }, 0);
      };
      document.body.addEventListener("touchend", resume, false);
    }
    step = 0;

    return ctx;
  } catch (e) {
    usingWebAudio = false;
    alert("Sorry, Web Audio API is not supported in this browser");
    return null;
  }
}

function drawBackground() {
  bctx.drawImage(bk, b.width / 2 - bk.width / 2, 0, bk.width, b.height);
}

function drawTarget() {
  tctx.clearRect(0, 0, b.width, b.height);
  tctx.drawImage(tank, tx, t.height + 5 - targetSzy, targetSzx, targetSzy);
}

function getRandomOffset(radius) {
  return {
    x: Math.random() * radius,
    y: Math.random() * radius,
  };
}
function animateImpact(x, y) {
  let nx = x + 0.5;
  let ny = y + 0.5;
  ictx.save();
  let amountOfPoints = 1200;
  for (let i = 0; i < amountOfPoints; i++) {
    let offset = getRandomOffset(pieceSzx * 10);

    setTimeout(() => {
      if (i <= 100) {
        ictx.globalAlpha = 1;
        ictx.fillStyle = "white";
      } else if (i <= 200) {
        ictx.globalAlpha = 1;
        ictx.fillStyle = "yellow";
      } else if (i <= 500) {
        ictx.globalAlpha = 1;
        ictx.globalAlpha = 0.75;
        ictx.fillStyle = "orange";
      } else if (i <= 900) {
        ictx.globalAlpha = 0.5;
        ictx.fillStyle = "red";
      } else if (i <= 1000) {
        ictx.globalAlpha = 0.3;
        ictx.fillStyle = "grey";
      } else {
        ictx.globalAlpha = 0.3;
        ictx.fillStyle = "darkgrey";
      }
      ictx.fillRect(
        nx + offset.x / 2 - 22,
        ny - 20 + offset.y - Math.floor(i / 25),
        5,
        5
      );

      setTimeout(() => {
        ictx.clearRect(
          nx + offset.x / 2 - 23,
          ny - 20 + offset.y - Math.floor(i / 25),
          7,
          7
        );
      }, 1.5 * i);
    }, 1 * i);
  }

  ictx.restore();
}

function drawPlayerPiece(x, y) {
  octx.strokeStyle = color;

  //if (y % 2 == 0) {
  octx.save();
  octx.globalAlpha = 0.2;
  let amountOfPoints = 4;

  for (let i = 0; i < amountOfPoints; i++) {
    let offset = getRandomOffset(pieceSzx);

    octx.fillStyle = octx.strokeStyle;
    octx.fillRect(x - offset.x / 2, y - offset.y, 4, 4);
  }

  octx.restore();
  //}

  sctx.clearRect(0, 0, s.width, s.height);
  sctx.save();
  sctx.translate(x, y);

  if (step == 0) {
    sctx.rotate(Math.PI / 180);
  } else {
    sctx.rotate((step * 2 * -15 * Math.PI) / 180);
  }

  sctx.drawImage(
    img,
    -pieceSzx / 2,
    -pieceSzy / 2 + Math.abs(step) * 4,
    pieceSzx,
    pieceSzy
  );
  sctx.restore();

  if (y == 0) {
    let old = o.width;
    o.width = 0;
    o.width = old;
    octx.clearRect(0, 0, o.width, o.height);
  }
}

var currKey = null;
function logKeyDown(e) {
  if (started) {
    switch (e.code) {
      case "ArrowLeft":
        if (step != 0.5) {
          step -= 0.5;
        } else {
          step = 0;
        }
        break;
      case "ArrowRight":
        if (step != -0.5) {
          step += 0.5;
        } else {
          step = 0;
        }
        break;
    }
  }
}

document.addEventListener("touchstart", handleTouchStart, false);

function handleTouchStart(evt) {
  let tx = evt.touches[0].clientX;
  console.log(tx);
  console.log(window.innerWidth);

  if (tx <= window.innerWidth / 2) {
    /* left swipe */
    if (step != 1) {
      step -= 0.5;
    } else {
      step = 0;
    }
  } else {
    if (step != -1) {
      step += 0.5;
    } else {
      step = 0;
    }
    /* right swipe */
  }
}

function reset() {
  life = Math.floor(25 * lvl);
  bombs = Math.floor((25 * lvl) / 2);
  tox = Math.floor(Math.random() * t.width - targetSzy);
  x = o.width / 2 - pieceSzy / 2;
  y = 0;
  tx = Math.floor(Math.random() * t.width - targetSzy);
  let old = o.width;
  o.width = 0;
  o.width = old;
  octx.clearRect(0, 0, o.width, o.height);
  drawPlayerPiece(x, y);
}

function handleStart() {
  drawBackground();
  scorebox.style.color = "white";

  started = !started;
  if (!started) {
    ol.style.visibility = "visible";
    startbtn.innerHTML = "Start";
    reset();
  } else {
    ol.style.visibility = "hidden";
    startbtn.innerHTML = "Stop";
  }

  o.focus();
}

function gameLoop(timeStamp) {
  var progress = new Date() - lastRender;
  console.log(timeStamp);

  if (bombs == 0 && life > 0) {
    scorebox.style.color = "red";
    scorebox.innerHTML = `* GAME OVER *`;
  }

  if (started && bombs > 0 && life > 0) {
    //if (tx == tox || tx >= b.width - targetSzx || tx <= 0) {
    if (tx >= b.width - targetSzx || tx <= 0) {
      tox = Math.floor(Math.random() * t.width - targetSzx);
    }

    if (tx < tox) {
      tx += Math.floor(progress / 3);
    } else {
      tx -= Math.floor(progress / 3);
    }

    if (x - pieceSzx / 2 <= 0) {
      step = 1;
    } else if (x >= o.width - pieceSzx / 2) {
      step = -1;
    }

    x += (progress / 10) * step;

    if (y < o.height && !hit()) {
      y += Math.floor(progress / 3);
    } else {
      if (hit()) {
        playHit();
        life -= 5;
        score += 10;
        if (tx < tox) {
          tox -= tox;
        } else {
          tox += tx;
        }
      }

      animateImpact(x, y);
      playMiss();
      y = 0;
      step = 0;
      bombs -= 1;
      color = colors[Math.floor(Math.random() * colors.length)];
    }

    scorebox.innerHTML = scoreBoxTemplate();
    fill.style.width = `${Math.floor((life / Math.floor(25 * lvl)) * 100)}%`;

    drawTarget();
    drawPlayerPiece(x, y);
  }

  if (life <= 0 && started) {
    bonus = bombs * 25;
    score += bonus;
    scorebox.innerHTML = "SUCCESS!";
    started = !started;

    setTimeout(function () {
      lvl = lvl + 1;
      scorebox.innerHTML = scoreBoxTemplate();
      setTimeout(() => {
        scorebox.innerHTML = `Prepare for Lvl ${lvl}`;

        setTimeout(() => {
          reset();
          handleStart();
        }, 2000);
      }, 2000);
    }, 3000);
  }

  lastRender = new Date();
  window.requestAnimationFrame(gameLoop);
}

function playHit() {
  for (var i = 0; i < 20; i++) {
    setTimeout(() => {
      playOSC(sounds[Math.floor(Math.random() * sounds.length)].tone, 0.2);
    }, 20 * i);
  }
}

function playMiss() {
  for (var i = 0; i < 10; i++) {
    setTimeout(() => {
      playOSC(sounds[Math.floor(Math.random() * sounds.length)].tone - 100);
    }, 20 * i);
  }
}

function hit() {
  return (
    x >= tx + pieceSzx / 2 &&
    x <= tx + pieceSzx / 2 + targetSzx &&
    y > o.height - targetSzy
  );
}

window.addEventListener("keydown", logKeyDown);
//window.addEventListener("keyup", logKeyUp);

var lastRender = new Date();
window.requestAnimationFrame(gameLoop);
