var delay_timer,storage=window.localStorage,versions={KJV:"kjv.json",ASV:"asv.json",CRTB:"crtb.json"},bible_data=[],search_data=[],ot_books=[],nt_books=[],current_book=[],current_chapter=[],current_version=null,current_book_chapters=[],show_books=!1,show_chapters=!1,ot=document.querySelector(".ot_books"),nt=document.querySelector(".nt_books"),book_btn=document.querySelector(".book_btn"),books_elm=document.querySelector(".books"),chapter_btn=document.querySelector(".chapter_btn"),chapters_elm=document.querySelector(".chapters"),loading_elm=document.querySelector(".loading"),data_elm=document.querySelector(".data"),results_elm=document.querySelector(".results"),search_input_elm=document.querySelector(".search input"),version_elm=document.querySelector(".version");function handle_pager_prev(){var e=parseInt(storage.getItem("current_chapter")),t=storage.getItem("current_book"),e=current_book_chapters.indexOf(e-1);0<=e?window.location.hash=`#${t}:`+(e+1):(console.log("prev: "+e),e=ot_books.indexOf(t),t=nt_books.indexOf(t),0<=e?ot_books[e-1]?(set_book(ot_books[e-1]),window.location.hash=`#${ot_books[e-1]}:`+current_book_chapters.length):(set_book(nt_books[nt_books.length-1]),window.location.hash=`#${nt_books[nt_books.length-1]}:`+current_book_chapters.length):nt_books[t-1]?(set_book(nt_books[t-1]),window.location.hash=`#${nt_books[t-1]}:`+current_book_chapters.length):(set_book(ot_books[ot_books.length-1]),window.location.hash=`#${ot_books[ot_books.length-1]}:`+current_book_chapters.length)),window.scrollTo(0,0)}function handle_pager_next(){var e=parseInt(storage.getItem("current_chapter")),t=storage.getItem("current_book"),e=current_book_chapters.indexOf(e+1);0<=e?window.location.hash=`#${t}:`+(e+1):(e=ot_books.indexOf(t),t=nt_books.indexOf(t),0<=e?ot_books[e+1]?window.location.hash=`#${ot_books[e+1]}:1`:window.location.hash=`#${nt_books[0]}:1`:nt_books[t+1]?window.location.hash=`#${nt_books[t+1]}:1`:window.location.hash=`#${ot_books[0]}:1`),window.scrollTo(0,0)}function handle_version(){console.log("setting version: "+version_elm.options[version_elm.selectedIndex].value),set_version(version_elm.options[version_elm.selectedIndex].value)}function handle_search(){clearTimeout(delay_timer),delay_timer=setTimeout(function(){let t=search_input_elm.value.toLowerCase();search_data=bible_data.filter(e=>JSON.stringify(e).toLowerCase().match(t)),2<t.length&&0<search_data.length?(data_elm.style.display="none",results_elm.style.display="block"):(data_elm.style.display="block",results_elm.style.display="none"),set_results()},1e3)}function reset_search(){search_data=[],search_input_elm.value="",results_elm.innerHTML="",data_elm.style.display="block",results_elm.style.display="none"}function set_results(){for(var t in results_elm.innerHTML="",search_data){let e=document.createElement("div");e.className="result";var o=`${search_data[t].book_name}:${search_data[t].chapter}:`+search_data[t].verse;e.innerHTML=`
            <div class="result-wrapper">
              <div class="result-text">
                <p>
                  ${search_data[t].text}
                </p>
              </div>
              <div class="result-details">
                <h3>
                  <a href="#${o}">
                    ${o}
                  </a>
                </h3>
              </div>
            </div>
          `,results_elm.appendChild(e)}}function toggle_books(){show_books=!show_books,show_chapters&&show_books&&toggle_chapters(),books_elm.style.display=show_books?"block":"none"}function toggle_chapters(){show_chapters=!show_chapters,show_books&&show_chapters&&toggle_books(),chapters_elm.style.display=show_chapters?"block":"none"}function set_loading(e){e?(loading_elm.style.display="block",data_elm.style.display="none"):(loading_elm.style.display="none",data_elm.style.display="block")}async function grab_data(){set_loading(!0);var e=storage.getItem("current_version")?storage.getItem("current_version"):"kjv.json";return await fetch(e).then(e=>e.json())}function set_chapter(t){for(var o in reset_search(),chapter_btn.innerHTML="<h2>"+t+"</h2>",storage.setItem("current_chapter",t),data_elm.innerHTML="",current_chapter=current_book.filter(e=>e.chapter==t)){let e=document.createElement("div");e.className="verse",e.innerHTML=`
            <div class="verse-wrapper">
              <div class="verse-number">
                <p>
                  <a href="#${current_chapter[o].book_name}:${current_chapter[o].chapter}:${current_chapter[o].verse}">
                  ${current_chapter[o].verse}
                  </a>
                </p>
              </div>

              <div class="verse-text">
                <p>
                  ${current_chapter[o].text}
                </p>
              </div>
            </div>
          `,data_elm.appendChild(e)}}function set_book(o){for(var e in reset_search(),book_btn.innerHTML="<h2>"+o+"</h2>",storage.setItem("current_book",o),current_book_chapters=[],current_book=bible_data.filter(e=>e.book_name==o))1==current_book[e].verse&&current_book_chapters.push(current_book[e].chapter);chapters_elm.innerHTML="";for(let t in current_book_chapters){let e=document.createElement("button");e.innerHTML="<h2>"+current_book_chapters[t]+"</h2>",e.onclick=()=>{window.location.hash=`#${o}:`+(parseInt(t)+1),toggle_chapters()},chapters_elm.appendChild(e)}set_chapter(current_book_chapters[0])}function set_version(e){storage.setItem("current_version",e),current_version=storage.getItem("current_version"),init()}function setup_version_options(){var t,e=storage.getItem("current_version");for(t in e&&"undefined"!=e&&null!=e||storage.setItem("current_version",versions.KJV),current_version=storage.getItem("current_version"),version_elm.innerHTML="",versions){let e=document.createElement("option");e.value=versions[t],e.innerHTML=t,version_elm.appendChild(e)}for(var o,r=0;o=version_elm.options[r];r++)if(o.value==current_version){version_elm.selectedIndex=r;break}console.log(versions)}function init(){window.addEventListener("popstate",handle_param),grab_data().then(e=>{if(bible_data=e,ot_books.length<=0){for(var t in e)1==e[t].chapter&&1==e[t].verse&&(ot_books.length<39?ot_books:nt_books).push(e[t].book_name);for(let t in ot_books){let e=document.createElement("button");e.innerHTML="<h2>"+ot_books[t]+"<h2>",e.onclick=()=>{set_book(ot_books[t]),toggle_books()},ot.appendChild(e)}for(let t in nt_books){let e=document.createElement("button");e.innerHTML="<h2>"+nt_books[t]+"</h2>",e.onclick=()=>{set_book(nt_books[t]),toggle_books()},nt.appendChild(e)}}var o=storage.getItem("current_book")?storage.getItem("current_book"):"Genesis",r=storage.getItem("current_chapter")?storage.getItem("current_chapter"):1;set_book(o),set_chapter(r),setup_version_options(),set_loading(!1),handle_param()})}function formatBookName(e){let t=e.replace(/%20/g," ");var e=ot_books.filter(e=>e.toLowerCase()==t.toLowerCase()),o=nt_books.filter(e=>e.toLowerCase()==t.toLowerCase());return console.log("book:"+t),console.log("ot_search:"+e),console.log("nt_search:"+o),0<e.length?e[0]:0<o.length?o[0]:""}function handle_param(){let n=window.location.hash.split("#");if(1<n.length){console.log("parsing...");var s=n[1].split(":");let t=formatBookName(s[0]),o=s[1],r=s[2],e=(set_book(t),set_chapter(parseInt(o)||1),bible_data.filter(e=>e.book_name==t));s=e.filter(e=>e.verse==parseInt(r)&&e.chapter==parseInt(o));if(0<s.length){data_elm.innerHTML="";let e=document.createElement("div");e.className="verse",e.innerHTML=`
              <div class="verse-wrapper">
                <div class="verse-number">
                  <p>
                    ${s[0].verse}
                  </p>
                </div>

                <div class="verse-text">
                  <p>
                    ${s[0].text}
                  </p>
                </div>
                <div class="space">
                </div>
                <div class="verse-details">
                  <h3>
                    ${s[0].book_name}
                    ${s[0].chapter}:${s[0].verse} (${s[0].translation_id})
                  </h3>
                </div>
              </div>
            `,data_elm.appendChild(e)}}}book_btn.onclick=()=>{toggle_books()},chapter_btn.onclick=()=>{toggle_chapters()},init();
