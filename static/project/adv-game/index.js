const canvas = document.getElementById('canvas')
const ctx = canvas.getContext('2d')
const debug = document.getElementById('debug')

ctx.fillStyle = "white"
ctx.fillRect(0,0,canvas.width,canvas.height)

var stop = false
var frameCount = 0
var fps, fpsInterval, startTime, now, then, elapsed

class Sprite {
	constructor({
		position,
		velocity,
		size,
		color = "red",
		imageSrc,
		scale = 1,
		framesMax = {x:1,y:1},
		currentFrame = {x:1,y:1}
	}) {
		this.position = position
		this.velocity = velocity
		this.framesMax = framesMax
		this.currentFrame = currentFrame
		this.size = size
		this.scale = scale
		this.color = color
		this.image = new Image()
		this.image.src = imageSrc
	}

	draw() {
		//ctx.fillStyle = this.color
		//ctx.fillRect(this.position.x, this.position.y, this.size.w * this.scale, this.size.h * this.scale)
		ctx.drawImage(
			this.image,
			0,0,
			this.image.width / this.framesMax.x,
			this.image.height/ this.framesMax.y,

			this.position.x,
			this.position.y,
			(this.image.width / this.framesMax.x) * this.scale,
			(this.image.height / this.framesMax.y) * this.scale
		)
	}
	update() {
		this.draw()
		this.position.y += this.velocity.y
		this.position.y += this.velocity.y
	}
}

const player = new Sprite({
	position: {x: 50, y: 50},
	velocity: {x: 0, y: 0},
	size: { w: 48, h: 37},
	framesMax: {x: 7, y: 12},
	scale: 2,
	color: "black",
	imageSrc: "./adventurer-v1.5-Sheet.png"
})

function startAnimating(fps) {
  fpsInterval = 1000 / fps
  then = window.performance.now()
  startTime = then
  console.log(startTime)
  run()
}

function run(newtime) {
  requestAnimationFrame(run)

  now = newtime
  elapsed = now - then
  if (elapsed > fpsInterval) {
  	player.update()
    then = now - (elapsed % fpsInterval)
    var sinceStart = now - startTime
    var currentFps =
      Math.round((1000 / (sinceStart / ++frameCount)) * 100) / 100
    debug.textContent = "debug: " + currentFps + " fps."
    //ctx.clearRect(0, 0, canvas.width, canvas.height)
    //f.fillStyle = "#151516"
    //f.fillRect(0, overlay.height - groundOffset, overlay.width, groundOffset)
    //f.strokeStyle = "black"
    //f.strokeRect(0, overlay.height - groundOffset, overlay.width, groundOffset)
  }
}

startAnimating(60)