const canvas = document.getElementById("main")
const back = document.getElementById("back")
const overlay = document.getElementById("overlay")
const info = document.getElementById("info")
const fore = document.getElementById("fore")
const wrapper = document.getElementById("wrapper")
const start = document.getElementById("start")
const level = document.getElementById("level-content")
const ammo = document.getElementById("ammo-content")
const debug = document.getElementById("debug-content")
const progress = document.getElementById("progress-current")

const c = canvas.getContext("2d")
const b = back.getContext("2d")
const o = overlay.getContext("2d")
const ic = info.getContext("2d")
const f = fore.getContext("2d")

const ratio = Math.floor(canvas.width / canvas.height)
const groundOffset = Math.floor(ratio * 10)
console.log(ratio)

back.width = wrapper.clientWidth
back.height = wrapper.clientHeight
canvas.width = wrapper.clientWidth
canvas.height = wrapper.clientHeight
overlay.width = wrapper.clientWidth
overlay.height = wrapper.clientHeight
info.width = wrapper.clientWidth
info.height = wrapper.clientHeight
fore.width = wrapper.clientWidth
fore.height = wrapper.clientHeight

var tank = new Image()
var bomb = new Image()
var bk = new Image()

const bkgrdUrl = "img/background.jpg"
const tankUrl = "img/tank.png"
const bombUrl = "img/bomb.png"

bk.onload = () => {
  b.drawImage(bk, back.width / 2 - bk.width / 2, 0, bk.width, back.height)
}

tank.src = tankUrl
bomb.src = bombUrl
bk.src = bkgrdUrl

let colors = ["green", "purple", "yellow", "brown", "orange", "blue", "pink"]

function getEnemies(n) {
  let e = []
  while (e.length < n) {
    e.push({
      img: tank,
      position: {
        x: Math.floor((Math.random() * canvas.width) / 2),
        y: Math.floor(canvas.height - 12 * ratio - groundOffset),
      },
      size: { width: 24 * ratio, height: 12 * ratio },
      velocity: { x: Math.abs(Math.random() * 4) + 1, y: 0 },
      hp: Math.floor(Math.random() * 20) + 1,
      color: colors[Math.floor(Math.random() * colors.length)],
    })
  }
  return e
}

class Sprite {
  constructor({ position, velocity, size, color = "red", img }) {
    this.img = img
    this.position = position
    this.velocity = velocity
    this.size = size
    this.color = color
  }

  draw() {
    if (this.img) {
      c.drawImage(
        this.img,
        this.position.x,
        this.position.y,
        this.size.width,
        this.size.height
      )
    } else {
      c.fillStyle = this.color
      c.fillRect(
        this.position.x,
        this.position.y,
        this.size.width,
        this.size.height
      )
    }
  }

  update() {
    this.draw()

    this.position.x += this.velocity.x
    this.position.y += this.velocity.y
  }
}

function boxCollide({ box1, box2 }) {
  return (
    box1.position.x >= box2.position.x - box1.size.width &&
    box1.position.x <= box2.position.x + box1.size.width + box2.size.width &&
    box1.position.y + box1.size.height >
      canvas.height - box2.size.height - groundOffset &&
    !box2.takingDamage
  )
}

class Game {
  constructor({ level, player }) {
    this.nextlvl = false
    this.gameover
    this.started
    this.paused
    this.enemies = []
    this.total_hp = 0
    this.current_hp = 0
    this.level = level
    this.player = player
  }

  loadLevel() {
    //let level = levels[this.level]
    let level = {
      player: { ammo: 1 * this.level + 5 },
      enemies: getEnemies(this.level + 1),
    }
    this.player.ammo = level.player.ammo
    this.player.position.y = -10
    this.player.velocity.x = 0
    this.enemies = []
    this.total_hp = 0
    this.current_hp = 0

    for (let i = 0; i < level.enemies.length; i++) {
      let e = new Enemy(level.enemies[i])
      this.enemies.push(e)
      this.total_hp += e.hp
    }
    this.current_hp = this.total_hp
    this.nextlvl = false
    o.clearRect(0, 0, info.width, info.height)
  }

  enemiesDefeated() {
    let d = true

    for (let i = 0; i < this.enemies.length; i++) {
      if (this.enemies[i].hp > 0) {
        d = false
      }
    }

    return d
  }

  update() {
    ic.clearRect(0, 0, info.width, info.height)

    this.current_hp = 0
    for (let i = 0; i < this.enemies.length; i++) {
      this.current_hp += this.enemies[i].hp
    }

    level.textContent = this.level
    ammo.textContent = this.player.ammo
    let percent = (this.current_hp / this.total_hp) * 100
    progress.style.width = percent + "%"

    if (
      this.started &&
      !this.paused &&
      this.player.ammo > 0 &&
      !this.enemiesDefeated()
    ) {
      this.player.update()
      for (var i = 0; i < this.enemies.length; i++) {
        this.enemies[i].update()

        if (boxCollide({ box1: this.player, box2: this.enemies[i] })) {
          let curr_hp = this.enemies[i].hp > 0
          this.enemies[i].takeDamage()

          if (curr_hp) {
            animateImpact(
              this.enemies[i].position.x,
              this.enemies[i].position.y,
              Math.floor(this.enemies[i].size.width),
              Math.floor(this.enemies[i].size.height)
            )
          }
        }
      }
    }

    if (this.player.ammo >= 0 && this.enemiesDefeated()) {
      this.nextlvl = true

      let lineHeight = Math.floor(info.height / 24)
      ic.font = `${lineHeight}px Comic Sans MS`

      ic.textBaseline = "middle"
      ic.textAlign = "center"
      ic.fillStyle = "white"
      ic.fillText(
        "great!",
        Math.floor(info.width / 2),
        Math.floor(info.height / 2)
      )
      ic.fillText(
        "press enter for next level",
        Math.floor(info.width / 2),
        Math.floor(info.height / 2 + lineHeight)
      )
    }

    if (this.started && this.player.ammo <= 0 && !this.enemiesDefeated() > 0) {
      this.gameover = true
      let lineHeight = Math.floor(info.height / 24)
      ic.font = `${lineHeight}px Comic Sans MS`

      ic.textBaseline = "middle"
      ic.textAlign = "center"
      ic.fillStyle = "white"
      ic.fillText(
        "game over.",
        Math.floor(info.width / 2),
        Math.floor(info.height / 2)
      )
      ic.fillText(
        "press enter to restart",
        Math.floor(info.width / 2),
        Math.floor(info.height / 2 + lineHeight)
      )
    }

    if (!this.started) {
      ic.font = `${Math.floor(info.height / 24)}px Comic Sans MS`

      ic.textBaseline = "middle"
      ic.textAlign = "center"
      ic.fillStyle = "white"
      ic.fillText(
        "press enter to start",
        Math.floor(info.width / 2),
        Math.floor(info.height / 2)
      )
    }

    if (this.started && this.paused) {
      ic.font = `${Math.floor(info.height / 24)}px Comic Sans MS`

      ic.textBaseline = "middle"
      ic.textAlign = "center"
      ic.fillStyle = "white"
      ic.fillText(
        "* paused *",
        Math.floor(info.width / 2),
        Math.floor(info.height / 2)
      )
    }
  }
}

class Player extends Sprite {
  constructor(props) {
    super(props)
    this.ammo = props.ammo
  }

  draw() {
    if (Math.abs(this.velocity.x != 0)) {
      c.save()
      c.clearRect(0, 0, canvas.width, canvas.height)
      c.fillStyle = this.color
      c.translate(this.position.x, this.position.y)
      c.rotate(Math.floor(this.velocity.x * -25 * Math.PI) / 180)
      if (this.img) {
        c.drawImage(
          this.img,
          -this.velocity.x,
          0,
          this.size.width,
          this.size.height
        )
      } else {
        c.fillRect(-this.velocity.x, 0, this.size.width, this.size.height)
      }
      c.restore()
    } else {
      super.draw()
    }

    if (this.position.y <= 0) {
      //o.clearRect(0, 0, overlay.width, overlay.height)
    }

    //if (Math.floor(this.position.y) % 4 == 0) {
    o.globalAlpha = 0.2
    o.fillStyle = "white"
    o.fillRect(
      Math.floor(this.position.x + this.size.width / 4),
      this.position.y,
      Math.floor(this.size.width / 2),
      Math.floor(this.size.width / 2)
    )
    //}
  }

  update() {
    this.draw()

    this.position.x += this.velocity.x
    this.position.y += this.velocity.y

    if (this.position.y + this.velocity.y >= canvas.height - groundOffset) {
      this.ammo -= 1
      animateImpact(
        this.position.x,
        canvas.height - this.size.height,
        Math.floor(this.size.width / 2),
        Math.floor(this.size.width / 2)
      )
      this.velocity.x = 0
      this.position.y = -this.size.height
    } else {
      this.position.y += this.velocity.y
    }

    if (
      this.position.x + this.size.width + this.velocity.x >= canvas.width ||
      this.position.x + this.velocity.x <= 0
    ) {
      this.velocity.x = -this.velocity.x
    }
  }
}

class Enemy extends Sprite {
  constructor(props) {
    super(props)
    this.hp = props.hp
    this.takingDamage
  }

  draw() {
    //let currColor = this.color

    //console.log(this.velocity.x)
    if (this.img) {
      let yOffset = this.hp <= 0 ? this.size.height / 2 : 0
      c.drawImage(
        this.img,
        this.position.x,
        this.position.y + yOffset,
        this.size.width,
        this.size.height
      )
    } else {
      //if (this.takingDamage || this.hp <= 0) {
      //  this.color = "red"
      //  c.globalAlpha = 0.3
      //}
      //super.draw()
      //this.color = currColor
      //c.font = `${this.size.height - 2}px Arial`
      //c.fillStyle = "black"
      //c.strokeStyle = "black"
      //c.fillText(
      //  this.hp,
      //  this.position.x + this.size.width / 3,
      //  this.position.y + this.size.height - 2
      //)
    }
  }

  update() {
    this.draw()
    if (this.hp > 0) {
      this.position.x += this.velocity.x
      if (this.position.x + this.size.width + this.velocity.x >= canvas.width) {
        this.velocity.x = -this.velocity.x
      } else if (this.position.x <= 0) {
        this.velocity.x = -this.velocity.x
      }

      let changeDir = Math.floor(Math.random(new Date()) * 200)

      if (changeDir === 0) {
        this.velocity.x = -this.velocity.x
      }
    }
  }

  takeDamage() {
    if (!this.takingDamage) {
      this.hp -= 10
      this.hp = this.hp < 0 ? 0 : this.hp
      this.takingDamage = true
    }
    setTimeout(() => {
      this.takingDamage = false
    }, 300)
  }
}

const game = new Game({
  level: 0,
  player: new Player({
    img: bomb,
    position: {
      x: Math.floor(canvas.width / 2),
      y: Math.floor(canvas.height + 4 * ratio),
    },
    size: { width: Math.floor(8 * ratio), height: Math.floor(16 * ratio) },
    velocity: { x: 0, y: 2 },
    ammo: 5,
    color: "black",
  }),
})

game.loadLevel()

var stop = false
var frameCount = 0
var fps, fpsInterval, startTime, now, then, elapsed

function startAnimating(fps) {
  fpsInterval = 1000 / fps
  then = window.performance.now()
  startTime = then
  console.log(startTime)
  run()
}

//let gstart, previousTimeStamp
//function run(timestamp) {
//  debug.textContent = canvas.height
//  if (gstart === undefined) {
//    gstart = timestamp
//  }

//  if (previousTimeStamp !== timestamp) {
//    c.clearRect(0, 0, canvas.width, canvas.height)
//    f.fillStyle = "#151516"
//    f.fillRect(0, overlay.height - groundOffset, overlay.width, groundOffset)
//    f.strokeStyle = "black"
//    f.strokeRect(0, overlay.height - groundOffset, overlay.width, groundOffset)
//    game.update()
//  }

//  previousTimeStamp = timestamp
//  requestAnimationFrame(run)
//}
//

function run(newtime) {
  requestAnimationFrame(run)

  now = newtime
  elapsed = now - then
  if (elapsed > fpsInterval) {
    then = now - (elapsed % fpsInterval)
    var sinceStart = now - startTime
    var currentFps =
      Math.round((1000 / (sinceStart / ++frameCount)) * 100) / 100
    debug.style.fontSize = "0.5em"
    debug.textContent = "debug: " + currentFps + " fps."
    c.clearRect(0, 0, canvas.width, canvas.height)
    f.fillStyle = "#151516"
    f.fillRect(0, overlay.height - groundOffset, overlay.width, groundOffset)
    f.strokeStyle = "black"
    f.strokeRect(0, overlay.height - groundOffset, overlay.width, groundOffset)
    game.update()
  }
}

startAnimating(60)

function animateImpact(x, y, width, height) {
  let amountOfPoints = 1200

  for (let i = 0; i < amountOfPoints; i++) {
    let ox = Math.floor(Math.random() * width)
    let oy = Math.floor(Math.random() * height)

    setTimeout(() => {
      if (i <= 100) {
        f.fillStyle = "white"
      } else if (i <= 200) {
        f.fillStyle = "yellow"
      } else if (i <= 500) {
        f.globalAlpha = 0.75
        f.fillStyle = "orange"
      } else if (i <= 900) {
        f.fillStyle = "red"
      } else if (i <= 1000) {
        f.fillStyle = "grey"
      } else {
        f.fillStyle = "darkgrey"
      }

      let size = Math.floor(ratio * 2)
      f.fillRect(x + ox, y + oy, size, size)

      setTimeout(() => {
        f.clearRect(x + ox - 1, y + oy - 1, size + 1, size + 1)
      }, 1.5 * i)
    }, 1 * i)
  }
}

function processStart() {
  if (game.started && !game.gameover && !game.nextlvl) {
    game.paused = !game.paused
  } else if (game.started && game.nextlvl) {
    game.level += 1
    game.loadLevel()
  } else {
    game.gameover = false
    game.loadLevel()
    game.started = true
  }

  start.textContent = game.started && !game.paused ? "pause" : "start"
}

function round(n) {
  return Math.sign(n) * Math.round(Math.abs(num))
}

window.addEventListener("keydown", (event) => {
  switch (event.key) {
    case "ArrowLeft":
      for (var i = 0; i < 5; i++) {
        setTimeout(() => {
          game.player.velocity.x -= 0.2
        }, i * 25)
      }
      break
    case "ArrowRight":
      for (var i = 0; i < 5; i++) {
        setTimeout(() => {
          game.player.velocity.x += 0.2
        }, i * 25)
      }
      break
    case "Enter":
      processStart()
      break
  }
})

document.addEventListener("touchstart", handleTouchStart, false)

function handleTouchStart(event) {
  let tx = event.touches[0].clientX
  console.log(tx)
  console.log(window.innerWidth)

  if (game.nextlvl || game.gameover) {
    processStart()
  } else if (game.started && !game.paused && !game.gameover) {
    if (tx <= window.innerWidth / 2) {
      for (var i = 0; i < 5; i++) {
        setTimeout(() => {
          game.player.velocity.x -= 0.2
        }, i * 25)
      }
    } else {
      for (var i = 0; i < 5; i++) {
        setTimeout(() => {
          game.player.velocity.x += 0.2
        }, i * 25)
      }
    }
  }
}
