class Sprite {
  constructor({ label, position, velocity, size, color = "red", img }) {
    this.img = img
    this.position = position
    this.velocity = velocity
    this.size = size
    this.color = color
  }

  draw() {
    c.fillStyle = this.color
    c.fillRect(
      this.position.x,
      this.position.y,
      this.size.width,
      this.size.height
    )
  }

  update() {
    this.draw()

    if (
      this.position.y + this.velocity.y >= 0 &&
      this.position.y + this.velocity.y + this.size.height <= canvas.height &&
      this.position.x + this.velocity.x >= 0 &&
      this.position.x + this.velocity.x + this.size.width <= canvas.width
    ) {
      this.position.y += this.velocity.y
      this.position.x += this.velocity.x
    }
  }
}

class Player extends Sprite {
  constructor(props) {
    super(props)
    this.hitting = false
    this.score = 0
    this.label = props.label
    this.ai = props.ai
  }

  update() {
    super.update()

    if (this.ai) {
      if (
        this.position.y + this.size.height / 2 >
        ball.position.y + ball.size.height / 2
      ) {
        this.velocity.y = -4
      } else if (
        this.position.y + this.size.height / 2 <
        ball.position.y + ball.size.height / 2
      ) {
        this.velocity.y = 4
      }
    }
  }
}

class Ball extends Sprite {
  constructor(props) {
    super(props)
    this.hitting = false
  }

  draw() {
    super.draw()
    u.save()
    u.fillStyle = "white"
    u.globalAlpha = 0.03
    let x = this.position.x
    let y = this.position.y
    let w = this.size.width
    let h = this.size.height

    u.fillRect(x, y, w, h)

    setTimeout(() => {
      u.clearRect(x - 1, y - 1, w + 2, h + 2)
    }, 1000)
    u.restore()
  }

  update() {
    super.update()

    let pos = 0
    if (
      this.position.x > 0 &&
      this.position.x <= player1.size.width * 2 &&
      this.position.y + this.size.height / 2 >= player1.position.y &&
      this.position.y - this.size.height / 2 <=
        player1.position.y + player1.size.height &&
      !this.hitting
    ) {
      this.hitting = true

      setTimeout(() => {
        this.hitting = false
      }, 500)

      pos = player1.size.height / 2 - (this.position.y - player1.position.y)
      this.velocity.x = -this.velocity.x
      this.velocity.y -= pos / 100
      return
    }

    if (
      this.position.x < canvas.width &&
      this.position.x + this.size.width >= player2.position.x &&
      this.position.y + this.size.height / 2 >= player2.position.y &&
      this.position.y - this.size.height / 2 <=
        player2.position.y + player2.size.height &&
      !this.hitting
    ) {
      this.hitting = true

      setTimeout(() => {
        this.hitting = false
      }, 500)

      pos = player2.size.height / 2 - (this.position.y - player2.position.y)
      this.velocity.x = -this.velocity.x
      this.velocity.y -= pos / 100
      return
    }

    if (
      this.position.y <= 0 ||
      this.position.y + this.size.height >= canvas.height
    ) {
      this.velocity.y = -this.velocity.y
    }

    if (
      this.position.x <= 0 ||
      this.position.x + this.size.width >= canvas.width
    ) {
      u.fillStyle = "white"
      u.fillRect(0, 0, underlay.width, underlay.height)
      setTimeout(() => {
        u.fillStyle = "black"
        u.fillRect(0, 0, underlay.width, underlay.height)
      }, 25)
      if (this.position.x <= 0) {
        player2.score += 1
        info.textContent = "p2 scores"
      } else {
        player1.score += 1
        info.textContent = "p1 scores"
      }

      this.position.x + this.size.width >= canvas.width
      this.velocity.x = -this.velocity.x
      //this.velocity.y = -this.velocity.y / 2
    }

    this.position.y += this.velocity.y
    this.position.x += this.velocity.x
  }
}
