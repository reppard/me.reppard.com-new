const canvas = document.getElementById("canvas")
const underlay = document.getElementById("underlay")
const p1 = document.getElementById("p1")
const p2 = document.getElementById("p2")
const info = document.getElementById("info")
const ai = document.getElementById("ai")
const c = canvas.getContext("2d")
const u = underlay.getContext("2d")
underlay.width = wrapper.clientWidth
underlay.height = wrapper.clientHeight
canvas.width = wrapper.clientWidth
canvas.height = wrapper.clientHeight

paddleHeight = Math.floor(canvas.height / 5)
paddleWidth = Math.floor(canvas.width / 50)

// Player 1
var player1 = new Player({
  label: "p1",
  position: { x: paddleWidth, y: canvas.height / 2 - paddleHeight / 2 },
  size: { width: paddleWidth, height: paddleHeight },
  velocity: { x: 0, y: 0 },
  color: "purple",
})

// Player 2
var player2 = new Player({
  label: "p2",
  position: {
    x: canvas.width - paddleWidth * 2,
    y: canvas.height / 2 - paddleHeight / 2,
  },
  size: { width: paddleWidth, height: paddleHeight },
  velocity: { x: 0, y: 0 },
  color: "blue",
})

var ball
let pause = false

function reset() {
  player1.score = 0
  player1.position = { x: paddleWidth, y: canvas.height / 2 - paddleHeight / 2 }
  player1velocity = { x: 0, y: 0 }
  player2.score = 0
  player2.position = {
    x: canvas.width - paddleWidth * 2,
    y: canvas.height / 2 - paddleHeight / 2,
  }
  player2velocity = { x: 0, y: 0 }

  let xv =
    Math.floor(Math.random() * 5) - 2 <= 0
      ? -Math.ceil(paddleWidth / 7)
      : Math.ceil(paddleWidth / 7)

  // Ball
  ball = new Ball({
    position: { x: canvas.width / 2, y: canvas.height / 2 },
    velocity: {
      x: xv == 0 ? xv + Math.ceil(paddleWidth / 8) : xv,
      y: Math.floor(Math.random() * 5) - 2,
    },
    size: { width: paddleWidth, height: paddleWidth },
    color: "green",
  })

  console.log(ball.velocity.x)

  info.textContent = ""
  pause = false
}

reset()

let start, previousTimeStamp

function run(timestamp) {
  if (start === undefined) {
    start = timestamp
  }

  if (previousTimeStamp !== timestamp) {
    if (!pause) {
      c.clearRect(0, 0, canvas.width, canvas.height)

      player1.update()
      player2.update()
      ball.update()
    }

    p1.textContent = player1.score
    p2.textContent = player2.score

    if (player1.score + player2.score >= 10 && player1.score != player2.score) {
      pause = true
      let winner = player1.score > player2.score ? player1 : player2

      info.textContent = `${winner.label} wins with ${winner.score}`
    }
  }

  previousTimeStamp = timestamp
  requestAnimationFrame(run)
}

function toggleAi() {
  player2.ai = !player2.ai
  ai.textContent = player2.ai ? "AI On" : "AI Off"
  reset()
}

requestAnimationFrame(run)
