---
title: "Envme"
date: 2020-04-17T15:32:24-04:00
description: "A Ruby Gem wrapper to configure env vars from HashiCorps' Consul"
externalLink: "https://github.com/reppard/envme"
repo: "https://github.com/reppard/envme"
tags: [projects, ruby, gem]
ShowReadingTime: false
ShowWordCount: false
---
