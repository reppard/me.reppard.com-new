---
title: "Namecraft"
date: 2014-08-18
description: "A Minecraft Username and/or Metal Band Name generator"
externalLink: "/project/namecraft"
repo: "https://gitlab.com/reppard/nameCraft"
tags: [projects, javascript]
ShowReadingTime: false
ShowWordCount: false
height: "80vh"
---

{{< vueobject "/project/namecraft" >}}
