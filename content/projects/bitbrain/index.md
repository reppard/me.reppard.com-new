---
title: "BitBrain.life"
date: 2022-05-20T21:48:53-04:00
description: "BitBrain.life aims to push forward learning by doing, inspiring
creativity and curiosity in technology."
externalLink: "https://bitbrain.life"
repo: ""
tags: [projects,javascript,strapi,vue,golang]
cover:
    image: "brain.jpg"
    linkFullImages: true
---

This project started as a simple "shower thought". Could I build jsFiddle
inside jsFiddle?

After a few minutes of "fiddling", it was clear that the answer was Yes!
Below is the crude code of that first iteration. No idea for how long, but the
original fiddle can be found [HERE](https://jsfiddle.net/reppard/p4r98ajs/39/).

## Prototype Code

### HTML

```html
<button id="run" onclick="runContent()">run</button>

<div class="box">
    <div class="col">
        <textarea id="html"></textarea>
        <textarea id="javascript"></textarea>
    </div>
    <div class="col">
        <textarea id="css"></textarea>
        <iframe src="" id="frame"> </iframe>
    </div>
</div>
```

### Javascript

```javascript
var frame = document.getElementById("frame");
var mu = document.getElementById("html");
var js = document.getElementById("javascript");
var css = document.getElementById("css");

function runContent() {
    frame.src = "data:text/html;charset=utf-8," + getContent();
}

function getContent() {
    return `
  <html>
  	<body>
  		${mu.value}
  	<\/body>
    <script>
  		${js.value}
  	<\/script>
    <style>
  		${css.value}
  	<\/style>
  <\/html>`;
}
```

### CSS

```css
textarea {
    height: 300px;
    width: 100%;
}

iframe {
    position: relative;
    margin: 0;
    padding: 0;
    height: 300px;
    width: 100%;
}

.box {
    display: grid;
    grid-column-gap: 8px;
    grid-template-columns: auto auto;
}

.col {
    display: grid;
    grid-row-gap: 8px;
    grid-template-rows: auto auto;
}
```

## Frontend

Now there are some obvious limitations with this implementation. For starters,
there is no syntax highlighting. It is a very crude implementation. But more
important that any of that is that it worked. I was able to effectively
reverse engineer a concept used for many sandbox sites like
[`jsFiddle`](https://jsfiddle.net) and `codepen`. I have been a long time fan
of [`jsFiddle`](https://jsfiddle.net) and have utilized it to prototype many
projects.

### Sandbox(frontend)

With this prototype proofed out, the obvious next step was for me to build my
own prototyping/sandbox application. Using `Vue.js`, [`ace`](https://ace.c9.io/),
`highlight.js`, and other libraries to build out the sandbox implementation.

### Markdown

With the sandbox implemented, it was easy enough to create an in-browser
markdown editor and renderer.

## Backend

### Sandbox(backend)

To allow for persisting of sandbox code, I created a backend using
[strapi.io](https://strapi.io/). `Strapi` is a fantastic headless-CRM for
developing backend APIs very quickly.

### Codebox

I thought that it would be nice to extend `Bitbrain.life's` functionality to
allow for code compilation and and execution right in the web browser. I built
out a another backend service written in `Golang` to facilitate this.

Currently, it supports `Golang`, `Ruby`, `Python`, and `Nodejs`. As it is
currently implemented, it should be very simple to add support for other
languages in the future.

### Sockbox

Last but not least, I thought it would be awesome to expose a full-fledged
Linux terminal right in the browser. Similar to the `codebox`, I created a
backend service that will spin up a `docker` container and expose the I/O
to the frontend via WebSockets. I had never had a chance to utilize WebSockets
before so this was a great learning experience for me.

Checkout [Bitbrain.life](https://bitbrain.life) and feel free to send me
feedback at [reppard@sdf.org](mailto:reppard@sdf.org).
