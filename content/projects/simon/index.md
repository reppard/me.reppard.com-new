---
title: "Simeon"
date: 2022-02-09T17:46:22-05:00
preview_img: /images/projects/simon-clone.png
externalLink: "/project/simeon"
description: A simon clone written with Vue.js
tags: [projects, javascript, vue]
ShowReadingTime: false
ShowWordCount: false
height: "80vh"
cover:
    image: "simon.jpg"
    linkFullImages: true
---

[Simon Clone](https://mysterious-copy.surge.sh)

{{< vueobject "/project/simeon" >}}
