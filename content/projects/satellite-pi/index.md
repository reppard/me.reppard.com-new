---
title: "Satellite Pi"
date: 2016-12-10T18:30:01
description: "Raspberry Pi Zero in an NES Satellite"
preview_img: /images/projects/satellite-pi.jpg
externalLink: https://www.instructables.com/SatellitePi-Raspberry-Pi-Zero-in-an-NES-Satellite/
tags: [projects, raspberrypi]
ShowReadingTime: false
ShowWordCount: false
cover:
    image: "satellitepi.jpg"
    linkFullImages: true
---

[On Instructables](https://www.instructables.com/SatellitePi-Raspberry-Pi-Zero-in-an-NES-Satellite/)
