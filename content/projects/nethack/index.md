---
title: "Nethack"
date: 2020-05-23
description: ""
externalPath: ""
repo: "https://gitlab.com/reppard/docker-nethack-server"
tags: [projects, nethack]
ShowReadingTime: false
ShowWordCount: false
cover:
    image: "nethack.jpg"
    linkFullImages: true
---

```
  $ telnet nh.reppard.com
```
