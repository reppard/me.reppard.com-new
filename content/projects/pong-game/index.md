---
title: "Pong Game"
date: 2022-07-01T01:14:06-04:00
description: "Pong style game written in Javascript+Canvas"
externalLink: "/project/pong-game"
tags: [projects, canvas, javascript]
ShowReadingTime: false
ShowWordCount: false
height: "400px"
cover:
    image: "pong.jpg"
    linkFullImages: true
---

Player1: A+Q, Player2: Up+Down

{{< vueobject "/project/pong-game" >}}
