---
title: "Bomb Game"
date: 2022-07-01T01:14:06-04:00
description: "Simple arcade style game written in Javascript+Canvas"
externalLink: "/project/bomb-game"
tags: [projects, canvas, javascript]
ShowReadingTime: false
ShowWordCount: false
height: "80vh"
cover:
    image: "bomb.jpg"
    linkFullImages: true
---

{{< vueobject "/project/bomb-game" >}}
