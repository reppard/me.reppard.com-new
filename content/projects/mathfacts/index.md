---
title: "Mathfacts"
date: 2016-09-22
description: "A simple game made to help my son with his mathfacts."
externalLink: "/project/mathfacts"
repo: ""
tags: [projects, javascript]
ShowReadingTime: false
ShowWordCount: false
height: "80vh"
cover:
    image: "math.jpg"
    linkFullImages: true
---

{{< vueobject "/project/mathfacts" >}}
