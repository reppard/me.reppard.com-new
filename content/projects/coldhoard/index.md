---
title: "ColdHoard"
date: 2021-05-04
preview_img: /images/projects/coldhoard.png
externalLink: "/project/coldhoard"
description: Client-side tool for tracking crypto wallets, written in Vue.js
tags: [projects, crypto, javascript, vue]
ShowReadingTime: false
ShowWordCount: false
height: "80vh"
cover:
    image: "crypto.jpg"
    linkFullImages: true
---

{{< vueobject "https://coldhoard.com" >}}
