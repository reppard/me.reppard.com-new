---
title: "Bible"
date: 2022-03-14T01:14:06-04:00
description: "Experiement building a KJV Bible app with Vanilla JS"
externalLink: "/project/bible"
repo: ""
tags: [projects, javascript, vanillajs]
ShowReadingTime: false
ShowWordCount: false
height: "80vh"
cover:
    image: "splash.png"
    linkFullImages: true
---

{{< vueobject "/project/bible" >}}
