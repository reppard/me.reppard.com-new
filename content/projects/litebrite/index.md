---
title: "Litebrite"
date: 2013-12-08
description: "JQuery lite brite"
externalLink: "/project/litebrite"
repo: ""
tags: [projects, javascript]
ShowReadingTime: false
ShowWordCount: false
height: "80vh"
cover:
    image: "litebrite.jpg"
    linkFullImages: true
---

{{< vueobject "/project/litebrite" >}}
