---
title: "Awssume"
date: 2014-04-01T15:24:26-04:00
description: "Authored this Ruby Gem while working at Manheim"
externalLink: "https://github.com/manheim/awssume"
repo: "https://github.com/manheim/awssume"
tags: [projects, ruby, gem]
ShowReadingTime: false
ShowWordCount: false
---
