---
date: "2022-02-09T16:47:41-05:00"
title: "First Hugo Post"
authors: ["reppard"]
tags: []
categories: []
externalLink: ""
series: []
---

This is the first post on this new space on the web. I've tried my hand at
blogging a few times over the years though it hasn't borne much fruit. I've
scraped old content from various places on the web and archived them here. It
is my intention to continue producing content(hopefully of greater quality of
what is currently found here). Wish me luck!
